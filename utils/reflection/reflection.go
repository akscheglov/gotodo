package reflection

import (
	"errors"
	"reflect"
	"strconv"
)

var NilValue = reflect.ValueOf(nil)

func ParseValue(t reflect.Type, value string) (val reflect.Value, err error) {
	var data interface{}
	switch t.Kind() {
	case reflect.Bool:
		data, err = strconv.ParseBool(value)
	case reflect.Float64:
		data, err = strconv.ParseFloat(value, 64)
	case reflect.Int:
		data, err = strconv.Atoi(value)
	case reflect.String:
		data = value
	default:
		err = errors.New("unsupported type")
	}

	val = reflect.ValueOf(data)
	return
}

func Ptr(v reflect.Value) reflect.Value {
	pt := reflect.PtrTo(v.Type()) // create a *T type.
	pv := reflect.New(pt.Elem())  // create a reflect.Value of type *T.
	pv.Elem().Set(v)              // sets pv to point to underlying value of v.
	return pv
}
