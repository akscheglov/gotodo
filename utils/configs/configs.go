package configs

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

func ReadConfFile(filename string, conf interface{}) interface{} {
	v := viper.New()
	v.AddConfigPath(".")

	mergeWith(v, filename)
	tryMergeWith(v, filename, os.Getenv("APP_ENV"))

	if err := v.Unmarshal(conf); err != nil {
		panic(fmt.Errorf("Fatal error loading config: %s \n", err))
	}

	return conf
}

func tryMergeWith(v *viper.Viper, filename string, env string) {
	if env == "" {
		env = "dev"
	}
	envFileName := filename + "." + env
	files, err := filepath.Glob(envFileName + "*")
	if err != nil {
		panic(err)
	}

	if len(files) > 0 {
		mergeWith(v, envFileName)
	}
}

func mergeWith(v *viper.Viper, filename string) {
	v.SetConfigName(filename)
	err := v.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file '%s': %s \n", filename, err))
	}
}
