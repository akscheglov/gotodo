package rw

import (
	"encoding/json"
	"golang.org/x/net/context"
	"io/ioutil"
	"net/http"
	"reflect"
)

func Respond(httpStatus int, w http.ResponseWriter, r *http.Request, data interface{}) {
	w.WriteHeader(httpStatus)

	v := reflect.ValueOf(data)
	if data != nil && (v.Kind() != reflect.Ptr || !v.IsNil()) {
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(data)
	}
}

func Error(status int, w http.ResponseWriter, r *http.Request, msg string) {
	http.Error(w, http.StatusText(status), status)
}

func ReadBody(r *http.Request, t reflect.Type) (val reflect.Value, err error) {
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	v := reflect.New(t)
	item := v.Interface()
	err = json.Unmarshal(reqBody, item)
	if err == nil {
		val = v.Elem()
	}

	return
}

func AddContextValue(r *http.Request, key interface{}, value interface{}) *http.Request {
	ctx := context.WithValue(r.Context(), key, value)
	return r.WithContext(ctx)
}
