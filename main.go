package main

import (
	"gotodo/infrastructure"
	"gotodo/infrastructure/db"
	"gotodo/infrastructure/di"
	"gotodo/infrastructure/logger"
)

func main() {
	log := logger.CreateLogger()
	engine := infrastructure.ConfigureServices(log)
	defer engine.Destroy()

	migrateDatabase(engine)

	srv := infrastructure.CreateServer(engine)

	log.Warn().Msgf("Starting a server at %s...", srv.Addr)
	log.Fatal().Err(srv.ListenAndServe()).Msg("Cannot start server.")
}

func migrateDatabase(engine di.ServiceEngine) {
	(engine.Resolve(db.MigratorType).(*db.Migrator)).Migrate()
}
