package health

import (
	"github.com/gorilla/mux"
	"gotodo/utils/rw"
	"net/http"
)

func Register(r *mux.Router) {
	r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		rw.Respond(http.StatusOK, w, r, map[string]bool{"ok": true})
	})
}
