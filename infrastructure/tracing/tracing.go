package tracing

import (
	"github.com/google/uuid"
	"gotodo/utils/rw"
	"net/http"
)

type key int8

const (
	reqIdKey key = 1
	opIdKey  key = 2
)

const reqIdHeader = "Request-Id"
const opIdHeader = "Operation-Id"

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = addRequestId(w, r)
		r = addOperationId(w, r)
		next.ServeHTTP(w, r)
	})
}

func GetRequestId(r *http.Request) string {
	return r.Context().Value(reqIdKey).(string)
}

func GetOperationId(r *http.Request) string {
	return r.Context().Value(opIdKey).(string)
}

func generateId() string {
	return uuid.New().String()
}

func addRequestId(w http.ResponseWriter, r *http.Request) *http.Request {
	reqId := r.Header.Get(reqIdHeader)
	if reqId == "" {
		reqId = generateId()
	}
	w.Header().Set(reqIdHeader, reqId)
	return rw.AddContextValue(r, reqIdKey, reqId)
}

func addOperationId(w http.ResponseWriter, r *http.Request) *http.Request {
	opId := uuid.New().String()
	w.Header().Set(opIdHeader, opId)
	return rw.AddContextValue(r, opIdKey, opId)
}
