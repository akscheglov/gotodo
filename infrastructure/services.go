package infrastructure

import (
	"gotodo/features/todo"
	"gotodo/infrastructure/db"
	"gotodo/infrastructure/di"
	"gotodo/infrastructure/logger"
	"gotodo/infrastructure/validator"
)

func ConfigureServices(log logger.Logger) di.ServiceEngine {
	return di.NewBuilder().
		AddSingletonConstructor(AppConfigType, newConfig, di.Noop).
		AddSingletonConstructor(HostConfigType, getHostConfig, di.Noop).
		AddSingletonConstructor(db.DatabaseConfigType, getDatabaseConfig, di.Noop).
		AddSingletonConstructor(db.DatabaseType, db.New, db.Close).
		AddSingletonConstructor(db.MigratorType, db.NewMigrator, di.Noop).
		AddSingletonConstructor(validator.ValidatorType, validator.New, di.Noop).
		AddSingletonConstructor(todo.StorageType, todo.NewDbStorage, di.Noop).
		AddScopedConstructor(logger.Type, func() logger.Logger { return log.Sublogger() }, di.Noop).
		AddScopedConstructor(todo.ServiceType, todo.New, di.Noop).
		Build()
}
