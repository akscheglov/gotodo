package validator

type Validator interface {
	Validate(obj interface{}) (err []ValidationError, ok bool)
}

type ValidationError struct {
	Namespace string `json:"namespace"`
	Field     string `json:"field"`
	Tag       string `json:"tag"`
}
