package validator

import (
	"gotodo/infrastructure/routing"
	"net/http"
	"reflect"
)

func CreateInterceptor(factory func(*http.Request) Validator) routing.Interceptor {
	return func(c *routing.CallContext, w http.ResponseWriter, r *http.Request) {
		validator := factory(r)
		if err, ok := validate(c, validator); !ok {
			c.SetResult(routing.NewObjectResult(http.StatusUnprocessableEntity, err))
		}
	}
}

func validate(c *routing.CallContext, validator Validator) (result map[string][]ValidationError, ok bool) {
	ok = true
	result = map[string][]ValidationError{}

	for _, param := range c.GetParameters() {
		if param.Kind() == reflect.Struct {
			errs, valid := validator.Validate(param.Interface())
			if !valid {
				result[param.Type().Name()] = errs
				ok = false
			}
		}
	}

	return
}
