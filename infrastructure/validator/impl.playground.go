package validator

import (
	"github.com/go-playground/validator/v10"
)

type validatorWrapper struct {
	validate *validator.Validate
}

func (v *validatorWrapper) Validate(obj interface{}) (errs []ValidationError, ok bool) {
	ok = true

	err := v.validate.Struct(obj)
	if err != nil {
		ok = false
		errs = convert(err)
	}

	return
}

func New() Validator {
	return &validatorWrapper{validator.New()}
}

func convert(err error) []ValidationError {
	var list []ValidationError

	// this check is only needed when your code could produce
	// an invalid value for validator such as interface with nil
	// value most including myself do not usually have code like this.
	if _, ok := err.(*validator.InvalidValidationError); ok {
		ok = false
	} else {
		errs := err.(validator.ValidationErrors)
		list = make([]ValidationError, len(errs))
		for i, err := range errs {
			list[i] = ValidationError{err.StructNamespace(), err.StructField(), err.Tag()}
		}
	}

	return list
}
