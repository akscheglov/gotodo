package validator

import (
	"gotodo/infrastructure/di"
	"reflect"
)

var ValidatorType = reflect.TypeOf((*Validator)(nil)).Elem()

func GetValidator(resolver di.Resolver) Validator {
	return resolver.Resolve(ValidatorType).(Validator)
}
