package infrastructure

import (
	"gotodo/infrastructure/db"
	"gotodo/utils/configs"
	"reflect"
)

type AppConfig struct {
	Host     *HostConfig
	Database *db.DatabaseConfig
}

const filename = "conf"

var AppConfigType = reflect.TypeOf((*AppConfig)(nil))

func newConfig() *AppConfig {
	conf := &AppConfig{}
	configs.ReadConfFile(filename, conf)
	return conf
}

func getHostConfig(conf *AppConfig) *HostConfig {
	return conf.Host
}

func getDatabaseConfig(conf *AppConfig) *db.DatabaseConfig {
	return conf.Database
}
