package routing

import (
	"errors"
	"github.com/gorilla/mux"
	"gotodo/utils/reflection"
	"gotodo/utils/rw"
	"net/http"
	"reflect"
)

var ParamNotFound = errors.New("required parameter not found")

// builder
type ParamsBuilder struct {
	params []routeParam
}

func (p *ParamsBuilder) Path(name string) *ParamsBuilder {
	add(p, pathParam{Name: name})
	return p
}

func (p *ParamsBuilder) Body() *ParamsBuilder {
	add(p, bodyParam{})
	return p
}

func (p *ParamsBuilder) Query(name string, required bool) *ParamsBuilder {
	add(p, queryParam{Name: name, Required: required})
	return p
}

func add(p *ParamsBuilder, param routeParam) {
	p.params = append(p.params, param)
}

type routeParam interface {
	Extract(r *http.Request, t reflect.Type) (val reflect.Value, err error)
}

// path
type pathParam struct {
	Name string
}

func (p pathParam) Extract(r *http.Request, t reflect.Type) (val reflect.Value, err error) {
	value, found := mux.Vars(r)[p.Name]

	if !found {
		err = ParamNotFound
		return
	}

	val, err = reflection.ParseValue(t, value)

	return
}

// query
type queryParam struct {
	Name     string
	Required bool
}

func (p queryParam) Extract(r *http.Request, t reflect.Type) (val reflect.Value, err error) {
	value, found := mux.Vars(r)[p.Name]

	if !found && p.Required {
		err = ParamNotFound
		return
	}

	val, err = reflection.ParseValue(t, value)

	return
}

// body
type bodyParam struct {
}

func (p bodyParam) Extract(r *http.Request, t reflect.Type) (val reflect.Value, err error) {
	return rw.ReadBody(r, t)
}
