package routing

import (
	"fmt"
	"github.com/gorilla/mux"
	"gotodo/infrastructure/di"
	"gotodo/utils/reflection"
	"net/http"
	"reflect"
)

func New(router *mux.Router, interceptors ...Interceptor) Router {
	return &routerImpl{router, interceptors}
}

type routerImpl struct {
	mux          *mux.Router
	interceptors []Interceptor
}

func (r *routerImpl) NewController(prefix string, service reflect.Type, interceptors ...Interceptor) Controller {
	intr := make([]Interceptor, len(r.interceptors)+len(interceptors), len(r.interceptors)+len(interceptors))
	copy(intr, r.interceptors)
	copy(intr[len(r.interceptors):], interceptors)
	sub := &controllerImpl{r.mux.PathPrefix(prefix).Subrouter(), service, intr}
	return sub
}

type controllerImpl struct {
	router       *mux.Router
	service      reflect.Type
	interceptors []Interceptor
}

func (s *controllerImpl) Register(route string, target string, httpMethod string, builder *ParamsBuilder) {
	if route != "" {
		route = fmt.Sprintf("/%s", route)
	}

	handler := makeHandler(s.service, s.interceptors, target, builder)

	s.router.HandleFunc(route, handler).Methods(httpMethod)
	route = fmt.Sprintf("%s/", route)
	s.router.HandleFunc(route, handler).Methods(httpMethod)
}

func makeHandler(service reflect.Type, interceptors []Interceptor, target string, params *ParamsBuilder) func(w http.ResponseWriter, r *http.Request) {
	method, ok := service.MethodByName(target)
	if !ok {
		panic("Method not found")
	}

	return createChain(service, interceptors, method, params)
}

func createChain(service reflect.Type, interceptors []Interceptor, method reflect.Method, builder *ParamsBuilder) func(http.ResponseWriter, *http.Request) {
	parameters := makeParametersBuilder(method, builder)
	invoker := makeActionInvoker(service, method)

	return func(w http.ResponseWriter, r *http.Request) {
		params, err := parameters(r)
		ctx := &CallContext{service, method, params, resultIfError(err)}

		for i := 0; i < len(interceptors) && ctx.result == nil; i++ {
			interceptors[i](ctx, w, r)
		}

		if ctx.result == nil {
			result, err := invoker(r, ctx.params)
			ctx.SetResult(createResult(result, err, r.Method))
		}

		ctx.result.Write(w, r)

		return
	}
}

func resultIfError(err error) ActionResult {
	if err != nil {
		return NewObjectResult(http.StatusUnprocessableEntity, err)
	} else {
		return nil
	}
}

func createResult(result reflect.Value, err error, method string) ActionResult {
	if err != nil {
		return NewObjectResult(http.StatusInternalServerError, err)
	} else if method == http.MethodPost {
		return NewObjectResult(http.StatusCreated, result.Interface())
	} else if result.Kind() == reflect.Invalid || (result.Kind() == reflect.Ptr && result.IsNil()) {
		return NewObjectResult(http.StatusNoContent, nil)
	} else {
		return NewObjectResult(http.StatusOK, result.Interface())
	}
}

func makeActionInvoker(service reflect.Type, method reflect.Method) func(*http.Request, []reflect.Value) (reflect.Value, error) {
	response := makeResponseBuilder(method)

	return func(r *http.Request, in []reflect.Value) (result reflect.Value, err error) {
		instance := di.GetResolver(r).Resolve(service)
		in[0] = reflect.ValueOf(instance)

		values := method.Func.Call(in)
		result, err = response(values)

		return
	}
}

func makeResponseBuilder(v reflect.Method) func(values []reflect.Value) (reflect.Value, error) {
	switch v.Type.NumOut() {
	case 0:
		return func(values []reflect.Value) (reflect.Value, error) {
			return reflection.NilValue, nil
		}

	case 1:
		return func(values []reflect.Value) (reflect.Value, error) {
			return values[0], nil
		}

	case 2:
		if !v.Type.Out(1).AssignableTo(reflect.TypeOf((*error)(nil)).Elem()) {
			panic("Invalid type of second return parameter. Error type expected")
		}
		return func(values []reflect.Value) (reflect.Value, error) {
			return values[0], values[0].Interface().(error)
		}

	default:
		panic("Too many return values")
	}
}

func makeParametersBuilder(v reflect.Method, builder *ParamsBuilder) func(*http.Request) ([]reflect.Value, error) {
	t := v.Type
	params := builder.params
	if t.NumIn()-1 != len(params) {
		panic("Unexpected number of handler parameters")
	}

	return func(r *http.Request) (in []reflect.Value, err error) {
		in = make([]reflect.Value, len(params)+1)
		for i := 1; i < len(in); i++ {
			in[i], err = params[i-1].Extract(r, t.In(i))
			if err != nil {
				return
			}
		}
		return
	}
}
