package routing

import (
	"gotodo/utils/rw"
	"net/http"
	"reflect"
)

type Interceptor func(c *CallContext, w http.ResponseWriter, r *http.Request)

type Router interface {
	NewController(prefix string, service reflect.Type, interceptors ...Interceptor) Controller
}

type Controller interface {
	Register(route string, target string, httpMethod string, builder *ParamsBuilder)
}

type ActionResult interface {
	Write(w http.ResponseWriter, r *http.Request)
}

type CallContext struct {
	service reflect.Type
	method  reflect.Method
	params  []reflect.Value
	result  ActionResult
}

func (ctx *CallContext) SetResult(result ActionResult) {
	ctx.result = result
}

func (ctx *CallContext) GetParameters() []reflect.Value {
	return ctx.params
}

func Params() *ParamsBuilder {
	return &ParamsBuilder{}
}

func NewObjectResult(statusCode int, obj interface{}) *ObjectResult {
	return &ObjectResult{statusCode, obj}
}

type ObjectResult struct {
	statusCode int
	obj        interface{}
}

func (res *ObjectResult) Write(w http.ResponseWriter, r *http.Request) {
	err, ok := res.obj.(error)
	if ok {
		panic(err)
	} else {
		rw.Respond(res.statusCode, w, r, res.obj)
	}
}
