package logger

import (
	"github.com/rs/zerolog"
	"io"
)

type filteredWriter struct {
	io.Writer
	level zerolog.Level
}

func (w *filteredWriter) WriteLevel(level zerolog.Level, p []byte) (n int, err error) {
	if level >= w.level {
		return w.Write(p)
	}
	return len(p), nil
}

type filteredHook struct {
	hook  zerolog.Hook
	level zerolog.Level
}

func (h *filteredHook) Run(e *zerolog.Event, level zerolog.Level, message string) {
	if level >= h.level {
		h.hook.Run(e, level, message)
	}
}
