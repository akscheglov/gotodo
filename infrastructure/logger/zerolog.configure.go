package logger

import (
	"github.com/rs/zerolog"
	"gotodo/utils/configs"
)

const filename = "zerolog"

func createZerologLogger() zerolog.Logger {
	conf := &zerologSettings{}
	configs.ReadConfFile(filename, conf)
	configurator := DefaultConfigurator()
	return configurator.CreateLogger(conf)
}
