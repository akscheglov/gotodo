package logger

import (
	"github.com/zenazn/goji/web/mutil"
	"net/http"
	"time"
)

type HttpLoggerEnrich func(log Logger, r *http.Request)

func Middleware(factory func(r *http.Request) Logger, enrichments ...HttpLoggerEnrich) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			log := factory(r)
			for _, enrich := range enrichments {
				enrich(log, r)
			}

			wp := mutil.WrapWriter(w)
			defer logEndExecuting(log, start, r, wp)
			log.Info().Msgf("Start HTTP %s %s executing...", r.Method, r.RequestURI)

			next.ServeHTTP(wp, r)
		})
	}
}

func logEndExecuting(log Logger, start time.Time, r *http.Request, wp mutil.WriterProxy) {
	var lvl LogContainer
	status := wp.Status()

	switch {
	case status > 499:
		lvl = log.Error()
	case status > 399:
		lvl = log.Warn()
	default:
		lvl = log.Info()
	}

	lvl.Msgf(
		"End HTTP %s %s responded %d in %d ms with %d bytes",
		r.Method,
		r.RequestURI,
		status,
		time.Since(start).Milliseconds(),
		wp.BytesWritten())
}
