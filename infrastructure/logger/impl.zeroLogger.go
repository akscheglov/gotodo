package logger

import "github.com/rs/zerolog"

// logger implementation

type zeroLogger struct {
	log zerolog.Logger
}

func (l *zeroLogger) Enrich(key string, value string) Logger {
	l.log.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str(key, value)
	})
	return l
}

func (l *zeroLogger) Sublogger() Logger {
	return &zeroLogger{l.log.With().Logger()}
}

func (l *zeroLogger) Trace() LogContainer {
	return &zeroContainer{l.log.Trace()}
}

func (l *zeroLogger) Debug() LogContainer {
	return &zeroContainer{l.log.Debug()}
}

func (l *zeroLogger) Info() LogContainer {
	return &zeroContainer{l.log.Info()}
}

func (l *zeroLogger) Warn() LogContainer {
	return &zeroContainer{l.log.Warn()}
}

func (l *zeroLogger) Error() LogContainer {
	return &zeroContainer{l.log.Error()}
}

func (l *zeroLogger) Fatal() LogContainer {
	return &zeroContainer{l.log.Fatal()}
}

// container implementation

type zeroContainer struct {
	e *zerolog.Event
}

func (c *zeroContainer) Msgf(format string, v ...interface{}) {
	c.e.Msgf(format, v...)
}

func (c *zeroContainer) Msg(msg string) {
	c.e.Msg(msg)
}

func (c *zeroContainer) Err(err error) LogContainer {
	c.e = c.e.Err(err)
	return c
}

func (c *zeroContainer) Str(key, val string) LogContainer {
	c.e = c.e.Str(key, val)
	return c
}

func (c *zeroContainer) Interface(key string, val interface{}) LogContainer {
	c.e = c.e.Interface(key, val)
	return c
}

func (c *zeroContainer) Bytes(key string, val []byte) LogContainer {
	c.e = c.e.Bytes(key, val)
	return c
}
