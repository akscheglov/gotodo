package logger

import (
	"gotodo/infrastructure/di"
	"net/http"
	"reflect"
)

var Type = reflect.TypeOf((*Logger)(nil)).Elem()

func GetFromRequest(r *http.Request) Logger {
	return get(di.GetResolver(r))
}

func get(resolver di.Resolver) Logger {
	return resolver.Resolve(Type).(Logger)
}
