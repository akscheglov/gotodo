package logger

// make logger compatible with zerolog interface but hide redundant

type Logger interface {
	Sublogger() Logger

	Enrich(key string, value string) Logger

	Trace() LogContainer
	Debug() LogContainer
	Info() LogContainer
	Warn() LogContainer
	Error() LogContainer
	Fatal() LogContainer
}

type LogContainer interface {
	Msgf(format string, v ...interface{})
	Msg(msg string)

	Err(err error) LogContainer
	Str(key, val string) LogContainer
	Interface(key string, i interface{}) LogContainer
	Bytes(key string, val []byte) LogContainer
}
