package logger

import (
	"github.com/halink0803/zerolog-graylog-hook/graylog"
	"github.com/rs/zerolog"
	"io"
	"os"
)

func DefaultConfigurator() *zerologConfigurator {
	return New(DefaultEnrichments(), DefaultWriters(), DefaultHooks())
}

func DefaultEnrichments() map[string]enrichmentFactory {
	return map[string]enrichmentFactory{
		"timestamp": func(with zerolog.Context, params map[string]interface{}) zerolog.Context {
			return with.Timestamp()
		},

		"caller": func(with zerolog.Context, params map[string]interface{}) zerolog.Context {
			skipFrameCount := int(params["skipFrameCount"].(float64))
			return with.CallerWithSkipFrameCount(skipFrameCount)
		},
	}
}

func DefaultWriters() map[string]writerFactory {
	return map[string]writerFactory{
		"console": func(params map[string]interface{}) io.Writer {
			return os.Stdout
		},

		"coloredConsole": func(params map[string]interface{}) io.Writer {
			return zerolog.ConsoleWriter{Out: os.Stdout}
		},

		"file": func(params map[string]interface{}) io.Writer {
			file, err := os.OpenFile(params["filepath"].(string), os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
			if err != nil {
				panic(err)
			}

			return file
		},
	}
}

func DefaultHooks() map[string]hookFactory {
	return map[string]hookFactory{
		"graylog": func(params map[string]interface{}) zerolog.Hook {
			hook, err := graylog.NewGraylogHook(params["address"].(string))
			if err != nil {
				panic(err)
			}

			return hook
		},
	}
}
