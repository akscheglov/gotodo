package logger

type zerologSettings struct {
	Level       string
	Enrichments []enrichmentConfig
	Writers     []writerConfig
	Hooks       []hookConfig
}

type enrichmentConfig struct {
	Type   string
	Params map[string]interface{}
}

type logConfig struct {
	Type   string
	Level  string
	Params map[string]interface{}
}
type writerConfig = logConfig
type hookConfig = logConfig
