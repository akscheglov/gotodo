package logger

import (
	"fmt"
	"github.com/rs/zerolog"
	"io"
)

type enrichmentFactory = func(log zerolog.Context, params map[string]interface{}) zerolog.Context
type writerFactory = func(params map[string]interface{}) io.Writer
type hookFactory = func(params map[string]interface{}) zerolog.Hook

func New(enrichments map[string]enrichmentFactory, writers map[string]writerFactory, hooks map[string]hookFactory) *zerologConfigurator {
	return &zerologConfigurator{
		Enrichments: enrichments,
		Writers:     writers,
		Hooks:       hooks,
	}
}

type zerologConfigurator struct {
	Enrichments map[string]enrichmentFactory
	Writers     map[string]writerFactory
	Hooks       map[string]hookFactory
}

func (c *zerologConfigurator) CreateLogger(conf *zerologSettings) zerolog.Logger {
	writer := zerolog.MultiLevelWriter(makeWriters(c, conf.Writers)...)

	log := zerolog.New(writer)

	log.Level(parseLevel(conf.Level))

	log = enrich(c, log, conf.Enrichments)
	log = addHooks(c, log, conf.Hooks)

	return log
}

func (c *zerologConfigurator) AddEnrichmentFactory(enrichmentType string, factory enrichmentFactory) *zerologConfigurator {
	c.Enrichments[enrichmentType] = factory
	return c
}

func (c *zerologConfigurator) AddWriterFactory(writerType string, factory writerFactory) *zerologConfigurator {
	c.Writers[writerType] = factory
	return c
}

func (c *zerologConfigurator) AddHookFactory(hookType string, factory hookFactory) *zerologConfigurator {
	c.Hooks[hookType] = factory
	return c
}

func addHooks(configurator *zerologConfigurator, log zerolog.Logger, hooks []hookConfig) zerolog.Logger {
	for _, hook := range hooks {
		configure, ok := configurator.Hooks[hook.Type]
		if !ok {
			panic(fmt.Sprintf("Unknown hook type '%s'", hook.Type))
		}

		log = log.Hook(&filteredHook{configure(hook.Params), parseLevel(hook.Level)})
	}
	return log
}

func enrich(configurator *zerologConfigurator, log zerolog.Logger, enrichments []enrichmentConfig) zerolog.Logger {
	with := log.With()

	for _, enrichment := range enrichments {
		configure, ok := configurator.Enrichments[enrichment.Type]
		if !ok {
			panic(fmt.Sprintf("Unknown enrichment type '%s'", enrichment.Type))
		}

		with = configure(with, enrichment.Params)
	}

	return with.Logger()
}

func makeWriters(configurator *zerologConfigurator, writers []writerConfig) []io.Writer {
	wrs := make([]io.Writer, len(writers))

	for i, writer := range writers {
		create, ok := configurator.Writers[writer.Type]
		if !ok {
			panic(fmt.Sprintf("Unknown writer type '%s'", writer.Type))
		}

		wrs[i] = &filteredWriter{create(writer.Params), parseLevel(writer.Level)}
	}

	return wrs
}

func parseLevel(level string) (lvl zerolog.Level) {
	lvl, err := zerolog.ParseLevel(level)
	if err != nil {
		panic(err)
	}
	return
}
