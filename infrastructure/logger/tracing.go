package logger

import (
	"gotodo/infrastructure/tracing"
	"net/http"
)

func EnrichWithRequestId(log Logger, r *http.Request) {
	log.Enrich("req_id", tracing.GetRequestId(r))
}

func EnrichWithOperationId(log Logger, r *http.Request) {
	log.Enrich("op_id", tracing.GetOperationId(r))
}
