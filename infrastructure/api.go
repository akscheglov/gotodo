package infrastructure

import (
	"github.com/gorilla/mux"
	"gotodo/controllers"
	"gotodo/infrastructure/di"
	"gotodo/infrastructure/routing"
	"gotodo/infrastructure/validator"
	"net/http"
)

func configureApi(handler *mux.Router) {
	handler = handler.PathPrefix("/api").Subrouter()

	router := routing.New(handler, validator.CreateInterceptor(validatorFactory))

	controllers.RegisterTo(router)
}

func validatorFactory(r *http.Request) validator.Validator {
	return validator.GetValidator(di.GetResolver(r))
}
