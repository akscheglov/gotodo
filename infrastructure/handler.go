package infrastructure

import (
	"github.com/gorilla/mux"
	"gotodo/infrastructure/di"
	"gotodo/infrastructure/health"
	"gotodo/infrastructure/logger"
	"gotodo/infrastructure/tracing"
	"gotodo/utils/rw"
	"net/http"
)

func createHandler(resolver di.ServiceEngine) http.Handler {
	handler := mux.NewRouter()

	handler.NotFoundHandler = createErrorHandler(http.StatusNotFound, "Requested resource not found")
	handler.MethodNotAllowedHandler = createErrorHandler(http.StatusMethodNotAllowed, "Method not allowed")

	health.Register(handler)

	registerMiddleware(handler, resolver)

	configureApi(handler)

	return handler
}

func createErrorHandler(status int, msg string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rw.Error(status, w, r, msg)
	})
}

func registerMiddleware(handler *mux.Router, resolver di.ServiceEngine) {
	chain := combine(
		di.Middleware(resolver),
		tracing.Middleware,
		logger.Middleware(
			logger.GetFromRequest,
			logger.EnrichWithRequestId,
			logger.EnrichWithOperationId),
		Recoverer)

	handler.Use(chain)
	handler.NotFoundHandler = chain(handler.NotFoundHandler)
	handler.MethodNotAllowedHandler = chain(handler.MethodNotAllowedHandler)
}

func combine(middlwares ...func(next http.Handler) http.Handler) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		for i := len(middlwares) - 1; i >= 0; i-- {
			next = middlwares[i](next)
		}
		return next
	}
}
