package infrastructure

import (
	"gotodo/infrastructure/logger"
	"gotodo/utils/rw"
	"net/http"
	"runtime/debug"
)

func Recoverer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rvr := recover(); rvr != nil {
				log := logger.GetFromRequest(r)
				log.Error().Interface("error", rvr).Bytes("debug_stack", debug.Stack()).Msg("Unexpected error occurred")
				rw.Error(http.StatusInternalServerError, w, r, "Unexpected error occurred")
			}
		}()

		next.ServeHTTP(w, r)
	})
}
