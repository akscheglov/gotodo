package db

import (
	"github.com/jinzhu/gorm"
	"reflect"
)

type DatabaseConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
	Debug    bool
}

var DatabaseConfigType = reflect.TypeOf((*DatabaseConfig)(nil))
var DatabaseType = reflect.TypeOf((*gorm.DB)(nil))

func New(config *DatabaseConfig) (db *gorm.DB, err error) {
	db, err = gorm.Open("sqlite3", config.DbName)
	if err == nil {
		// tbd: set custom logger
		// db.SetLogger(gorm.Logger{...})

		if config.Debug {
			db = db.Debug()
		}
	}
	return
}

func Close(instance interface{}) {
	_ = instance.(*gorm.DB).Close()
}
