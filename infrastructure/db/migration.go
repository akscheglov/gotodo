package db

import (
	"github.com/jinzhu/gorm"
	"gotodo/features/todo"
	"reflect"
)

var MigratorType = reflect.TypeOf((*Migrator)(nil))

type Migrator struct {
	db *gorm.DB
}

func (m *Migrator) Migrate() {
	m.db.AutoMigrate(&todo.Todo{})
}

func NewMigrator(db *gorm.DB) *Migrator {
	return &Migrator{db: db}
}
