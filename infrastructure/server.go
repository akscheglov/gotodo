package infrastructure

import (
	"fmt"
	"gotodo/infrastructure/di"
	"net/http"
	"reflect"
	"time"
)

type HostConfig struct {
	Port int
}

var HostConfigType = reflect.TypeOf((*HostConfig)(nil))

func CreateServer(resolver di.ServiceEngine) *http.Server {
	conf := resolver.Resolve(HostConfigType).(*HostConfig)
	srv := &http.Server{
		Handler:      createHandler(resolver),
		Addr:         fmt.Sprintf(":%d", conf.Port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	return srv
}
