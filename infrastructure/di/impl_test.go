package di

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"sync"
	"testing"
	"time"
)

var tstServiceType = reflect.TypeOf((*tstService)(nil))
var tstServiceWithPrivateType = reflect.TypeOf((*tstInvalidService)(nil))
var tstServiceInterfaceType = reflect.TypeOf((*tstServiceInterface)(nil)).Elem()
var strType = reflect.TypeOf((*string)(nil)).Elem()

type tstServiceInterface interface {
	Foo()
}

type tstInvalidService struct {
	bar string
}
type tstService struct {
	Bar string
}

func (t *tstService) Foo() {}

func createTstServiceNoParams() *tstService {
	return &tstService{Bar: "hello"}
}

func createTstServiceWithParam(bar string) *tstService {
	return &tstService{Bar: bar}
}

func TestSingletonInstance(t *testing.T) {
	someService := &tstService{Bar: "hello"}
	other := &tstService{Bar: "hello"}

	engine := NewBuilder().
		AddSingletonInstance(tstServiceType, someService).
		Build()

	service1 := engine.Resolve(tstServiceType).(*tstService)

	assert.Equal(t, someService, service1)
	assert.True(t, other != service1)
}

func TestSingletonConstructorNoParams(t *testing.T) {
	someService := &tstService{Bar: "hello"}

	engine := NewBuilder().
		AddSingletonConstructor(tstServiceType, createTstServiceNoParams, Noop).
		Build()

	service1 := engine.Resolve(tstServiceType).(*tstService)
	service2 := engine.Resolve(tstServiceType).(*tstService)
	service3 := engine.Scope().Resolve(tstServiceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)

	assert.Same(t, service1, service2)
	assert.Same(t, service1, service3)
	assert.True(t, service1 != someService)
}

func TestSingletonConstructorWithParam(t *testing.T) {
	someService := &tstService{Bar: "hello"}

	engine := NewBuilder().
		AddSingletonInstance(strType, "hello").
		AddSingletonConstructor(tstServiceType, createTstServiceWithParam, Noop).
		Build()

	service1 := engine.Resolve(tstServiceType).(*tstService)
	service2 := engine.Resolve(tstServiceType).(*tstService)
	service3 := engine.Scope().Resolve(tstServiceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)

	assert.Same(t, service1, service2)
	assert.Same(t, service1, service3)
	assert.True(t, service1 != someService)
}

func TestSingletonType(t *testing.T) {
	someService := &tstService{Bar: "hello"}

	engine := NewBuilder().
		AddSingletonInstance(strType, "hello").
		AddSingleton(tstServiceType, tstServiceType, Noop).
		Build()

	service1 := engine.Resolve(tstServiceType).(*tstService)
	service2 := engine.Resolve(tstServiceType).(*tstService)
	service3 := engine.Scope().Resolve(tstServiceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)

	assert.Same(t, service1, service2)
	assert.Same(t, service1, service3)
	assert.True(t, service1 != someService)
}

func TestScoped(t *testing.T) {
	engine := NewBuilder().
		AddSingletonInstance(strType, "hello").
		AddScoped(tstServiceType, tstServiceType, Noop).
		Build()

	scope1 := engine.Scope()
	scope2 := engine.Scope()

	service1 := scope1.Resolve(tstServiceType).(*tstService)
	service2 := scope1.Resolve(tstServiceType).(*tstService)
	service3 := scope2.Resolve(tstServiceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)

	assert.Same(t, service1, service2)
	assert.True(t, service1 != service3)
}

func TestTransient(t *testing.T) {
	engine := NewBuilder().
		AddSingletonInstance(strType, "hello").
		AddTransient(tstServiceType, tstServiceType, Noop).
		Build()

	scope1 := engine.Scope()
	scope2 := engine.Scope()

	service1 := engine.Resolve(tstServiceType).(*tstService)
	service2 := engine.Resolve(tstServiceType).(*tstService)
	service3 := scope1.Resolve(tstServiceType).(*tstService)
	service4 := scope1.Resolve(tstServiceType).(*tstService)
	service5 := scope2.Resolve(tstServiceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)

	assert.True(t, service1 != service2)
	assert.True(t, service3 != service4)
	assert.True(t, service1 != service3)
	assert.True(t, service5 != service1)
	assert.True(t, service5 != service3)
}

func TestInterface(t *testing.T) {
	engine := NewBuilder().
		AddTransientConstructor(tstServiceInterfaceType, createTstServiceNoParams, Noop).
		Build()

	service1 := engine.Resolve(tstServiceInterfaceType).(*tstService)

	assert.Equal(t, "hello", service1.Bar)
}

func TestDestroyEngine(t *testing.T) {
	engine := NewBuilder().
		AddSingletonConstructor(tstServiceInterfaceType, createTstServiceNoParams, func(instance interface{}) {
			instance.(*tstService).Bar = "destroyed"
		}).
		Build()

	service1 := engine.Resolve(tstServiceInterfaceType).(*tstService)
	assert.Equal(t, "hello", service1.Bar)

	engine.Destroy()
	assert.Equal(t, "destroyed", service1.Bar)
}

func TestDestroyScope(t *testing.T) {
	engine := NewBuilder().
		AddTransientConstructor(tstServiceInterfaceType, createTstServiceNoParams, func(instance interface{}) {
			instance.(*tstService).Bar = "destroyed"
		}).
		Build()

	scope := engine.Scope()

	service1 := scope.Resolve(tstServiceInterfaceType).(*tstService)
	assert.Equal(t, service1.Bar, "hello")

	scope.Destroy()
	assert.Equal(t, service1.Bar, "destroyed")
}

func TestSingletonCannotReferScopedInConstructor(t *testing.T) {
	builder := NewBuilder().
		AddScopedConstructor(strType, func() string { return "hello" }, Noop).
		AddSingletonConstructor(tstServiceType, createTstServiceWithParam, Noop)

	assert.Panics(t, func() {
		_ = builder.Build()
	})
}

func TestServiceNotRegistered(t *testing.T) {
	engine := NewBuilder().
		Build()

	assert.Panics(t, func() {
		_ = engine.Resolve(tstServiceType)
	})
}

func TestCannotBuildIfDependencyNotRegistered(t *testing.T) {
	builder := NewBuilder().
		AddSingletonConstructor(tstServiceType, createTstServiceWithParam, Noop)

	assert.Panics(t, func() {
		_ = builder.Build()
	})
}

func TestCannotBuildIfTypeHasPrivateFields(t *testing.T) {
	builder := NewBuilder().
		AddSingletonInstance(strType, "hello").
		AddSingleton(tstServiceWithPrivateType, tstServiceWithPrivateType, Noop)

	assert.Panics(t, func() {
		_ = builder.Build()
	})
}

func TestCannotBuildIfHasSelfDependence(t *testing.T) {
	builder := NewBuilder().
		AddSingletonConstructor(tstServiceType, func(s *tstService) *tstService { return s }, Noop)

	assert.Panics(t, func() {
		_ = builder.Build()
	})
}

func TestCannotBuildIfHasCircleDependence(t *testing.T) {
	builder := NewBuilder().
		AddSingletonConstructor(strType, func(s *tstService) string { return s.Bar }, Noop).
		AddSingletonConstructor(tstServiceType, createTstServiceWithParam, Noop)

	assert.Panics(t, func() {
		_ = builder.Build()
	})
}

func TestSingletonConcurrentResolving(t *testing.T) {
	engine := NewBuilder().
		AddSingletonConstructor(tstServiceType, func() *tstService {
			<-time.After(20 * time.Millisecond)
			return createTstServiceNoParams()
		}, Noop).
		Build()

	var service1 *tstService
	var service2 *tstService
	var service3 *tstService

	var wg sync.WaitGroup
	wg.Add(3)

	go func() { service1 = engine.Resolve(tstServiceType).(*tstService); wg.Done() }()
	go func() { service2 = engine.Resolve(tstServiceType).(*tstService); wg.Done() }()
	go func() { service3 = engine.Resolve(tstServiceType).(*tstService); wg.Done() }()

	wg.Wait()

	assert.Equal(t, "hello", service1.Bar)

	assert.Same(t, service1, service2)
	assert.Same(t, service1, service3)
}
