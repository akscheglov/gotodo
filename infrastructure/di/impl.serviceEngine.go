package di

import (
	"fmt"
	"reflect"
)

type instanceFactory = func(resolver *scopedResolver) (interface{}, error)
type dependenciesAccessor = func(reflect.Type) []reflect.Type

type serviceActivator struct {
	lifetime lifetime
	factory  instanceFactory
	destroy  func(instance interface{})
}

func createServiceEngine(services serviceMap) ServiceEngine {
	engine := &serviceEngine{}
	engine.scope = createScopedResolver(engine)
	engine.activators = createActivators(services)
	return engine
}

// service engine

type serviceEngine struct {
	scope      *scopedResolver
	activators map[reflect.Type]serviceActivator
}

func (e *serviceEngine) Resolve(service reflect.Type) interface{} {
	return e.scope.Resolve(service)
}

func (e *serviceEngine) Scope() ScopedResolver {
	// add to destroy?
	return createScopedResolver(e)
}

func (e *serviceEngine) Destroy() {
	e.scope.Destroy()
	e.activators = nil
	e.scope = nil
}

func (e *serviceEngine) getActivator(service reflect.Type) serviceActivator {
	if activator, ok := e.activators[service]; ok {
		return activator
	}
	panic(fmt.Sprintf("Requested service %s not found", service))
}

func createActivators(services serviceMap) map[reflect.Type]serviceActivator {
	activators := map[reflect.Type]serviceActivator{}
	for target, value := range services {
		factory := createInstanceFactory(target, value, services)
		activators[target] = serviceActivator{lifetime: value.lifetime, destroy: value.destroy, factory: factory}
	}
	return activators
}

func createInstanceFactory(target reflect.Type, descriptor serviceDescriptor, services serviceMap) instanceFactory {
	switch descriptor.activatorKind {
	case instance:
		return func(r *scopedResolver) (interface{}, error) { return descriptor.activator, nil }

	case constructor:
		return createInstanceFactoryForConstructor(target, descriptor, services)

	case implType:
		return createInstanceFactoryForType(target, descriptor, services)

	default:
		panic(fmt.Sprintf("Unknown activator kind '%v'", descriptor.activatorKind))
	}
}

// instance builders

func createInstanceFactoryForConstructor(target reflect.Type, descriptor serviceDescriptor, services serviceMap) instanceFactory {
	depends := ensureValidDependencies(target, descriptor, services, createDependenciesAccessor(services))
	act := reflect.ValueOf(descriptor.activator)

	return func(resolver *scopedResolver) (inst interface{}, err error) {
		defer func() {
			if rec := recover(); rec != nil {
				var ok bool
				err, ok = rec.(error)
				if !ok {
					err = fmt.Errorf("cannot build type %s: %v", target, rec)
				}
			}
		}()
		in := make([]reflect.Value, len(depends))

		for i := range in {
			var val interface{}
			val, err = resolve(resolver, depends[i])
			if err != nil {
				return
			}
			in[i] = reflect.ValueOf(val)
		}

		results := act.Call(in)
		inst = results[0].Interface()
		if len(results) == 2 && !results[1].IsNil() {
			err = results[1].Interface().(error)
		}

		return
	}
}

func createInstanceFactoryForType(target reflect.Type, descriptor serviceDescriptor, services serviceMap) instanceFactory {
	depends := ensureValidDependencies(target, descriptor, services, createDependenciesAccessor(services))
	impl, isPtr := getTargetType(descriptor)

	return func(resolver *scopedResolver) (inst interface{}, err error) {
		instance := reflect.New(impl)
		elem := instance.Elem()
		for i := range depends {
			var val interface{}
			val, err = resolve(resolver, depends[i])
			if err != nil {
				return
			}

			elem.Field(i).Set(reflect.ValueOf(val))
		}

		if isPtr {
			inst = instance.Interface()
		} else {
			inst = elem.Interface()
		}

		return
	}
}

func ensureValidDependencies(target reflect.Type, descriptor serviceDescriptor, services serviceMap, dependencies dependenciesAccessor) []reflect.Type {
	depends := dependencies(target)

	for _, dependency := range depends {
		if target == dependency {
			panic(fmt.Sprintf("Type %s depends on itself", target))
		}

		depends, ok := services[dependency]
		if !ok {
			panic(fmt.Sprintf("Dependency %s not found for type %s", dependency, target))
		}

		isInvalid := descriptor.lifetime != scoped && depends.lifetime == scoped
		if isInvalid {
			panic(fmt.Sprintf("The type %s of lifetime '%s' cannot depends on type %s of lifetime '%s'.", target, descriptor.lifetime, dependency, depends.lifetime))
		}
	}

	ensureNoCircle(target, createDependenciesAccessor(services), map[reflect.Type]bool{})

	return depends
}

func ensureNoCircle(target reflect.Type, dependencies dependenciesAccessor, visited map[reflect.Type]bool) {
	if visited[target] {
		panic(fmt.Sprintf("Circle detected: %v", visited))
	}

	visited[target] = true
	for _, dependency := range dependencies(target) {
		ensureNoCircle(dependency, dependencies, visited)
	}
	visited[target] = false
}

func createDependenciesAccessor(services serviceMap) dependenciesAccessor {
	return func(target reflect.Type) []reflect.Type {
		descriptor := services[target]

		switch descriptor.activatorKind {
		case instance:
			return make([]reflect.Type, 0)

		case constructor:
			act := reflect.ValueOf(descriptor.activator)
			depends := make([]reflect.Type, act.Type().NumIn())
			for i := range depends {
				depends[i] = act.Type().In(i)
			}

			return depends

		case implType:
			impl, _ := getTargetType(descriptor)
			depends := make([]reflect.Type, impl.NumField())
			instance := reflect.New(impl).Elem()
			for i := range depends {
				if !instance.Field(i).CanSet() {
					panic(fmt.Sprintf("Field '%s' of type %s cannot be set", impl.Field(i).Name, impl))
				}
				depends[i] = impl.Field(i).Type
			}

			return depends

		default:
			panic(fmt.Sprintf("Unknown activator kind '%v'", descriptor.activatorKind))
		}
	}
}

func getTargetType(descriptor serviceDescriptor) (impl reflect.Type, isPtr bool) {
	impl = descriptor.activator.(reflect.Type)
	isPtr = impl.Kind() == reflect.Ptr
	if isPtr {
		impl = impl.Elem()
	}
	return
}
