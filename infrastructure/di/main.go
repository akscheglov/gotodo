package di

import "reflect"

var Noop = func(instance interface{}) {}

type Resolver interface {
	Resolve(service reflect.Type) interface{}
}

type ServiceEngine interface {
	Resolve(service reflect.Type) interface{}
	Scope() ScopedResolver
	Destroy()
}

type ScopedResolver interface {
	Resolve(service reflect.Type) interface{}
	Destroy()
}

type ServiceCollection interface {
	// Specifies that a single instance of the service will be created.
	AddSingleton(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection
	AddSingletonConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection
	AddSingletonInstance(service reflect.Type, instance interface{}) ServiceCollection

	// Specifies that a new instance of the service will be created every time it is requested.
	AddTransient(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection
	AddTransientConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection

	// Specifies that a new instance of the service will be created for each scope.
	AddScoped(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection
	AddScopedConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection

	Build() ServiceEngine
}
