package di

import (
	"fmt"
	"reflect"
)

type serviceMap = map[reflect.Type]serviceDescriptor

type lifetime string
type activatorKind int
type serviceDescriptor struct {
	activator     interface{}
	activatorKind activatorKind
	lifetime      lifetime
	destroy       func(instance interface{})
}

const (
	// allowed single instance only
	singleton lifetime = "singleton"
	// single instance for each scope
	scoped lifetime = "scoped"
	// instance created for each call
	transient lifetime = "transient"

	instance    activatorKind = 1
	implType    activatorKind = 2
	constructor activatorKind = 3
)

// implement service collection

type serviceCollection struct {
	services serviceMap
}

func (b *serviceCollection) AddSingleton(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection {
	return addImplType(b, service, impl, singleton, destroy)
}

func (b *serviceCollection) AddSingletonConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection {
	return addConstructor(b, service, constructor, singleton, destroy)
}

func (b *serviceCollection) AddSingletonInstance(service reflect.Type, inst interface{}) ServiceCollection {
	return addSingleton(b, service, inst)
}

func (b *serviceCollection) AddTransient(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection {
	return addImplType(b, service, impl, transient, destroy)
}

func (b *serviceCollection) AddTransientConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection {
	return addConstructor(b, service, constructor, transient, destroy)
}

func (b *serviceCollection) AddScoped(service reflect.Type, impl reflect.Type, destroy func(instance interface{})) ServiceCollection {
	return addImplType(b, service, impl, scoped, destroy)
}

func (b *serviceCollection) AddScopedConstructor(service reflect.Type, constructor interface{}, destroy func(instance interface{})) ServiceCollection {
	return addConstructor(b, service, constructor, scoped, destroy)
}

func (b *serviceCollection) Build() ServiceEngine {
	return createServiceEngine(b.services)
}

// add to collection

func addImplType(b *serviceCollection, service reflect.Type, impl reflect.Type, scope lifetime, destroy func(instance interface{})) *serviceCollection {
	if !impl.AssignableTo(service) {
		panic(fmt.Sprintf("Type %s cannot be assigned to %s", impl, service))
	}

	if impl.Kind() != reflect.Struct && (impl.Kind() != reflect.Ptr || impl.Elem().Kind() != reflect.Struct) {
		panic(fmt.Sprintf("Type %s is non-struct type", impl))
	}

	b.services[service] = serviceDescriptor{impl, implType, scope, nilIfNoop(destroy)}
	return b
}

func addConstructor(b *serviceCollection, service reflect.Type, ctr interface{}, scope lifetime, destroy func(instance interface{})) *serviceCollection {
	val := reflect.ValueOf(ctr)
	if val.Kind() != reflect.Func {
		panic("Provided interface is not a function")
	}

	switch val.Type().NumOut() {
	case 1:
	case 2:
		if !val.Type().Out(1).AssignableTo(reflect.TypeOf((*error)(nil)).Elem()) {
			panic("Invalid type of second return parameter. Error type expected")
		}
	default:
		panic("Invalid number of output parameters count")
	}

	if !val.Type().Out(0).AssignableTo(service) {
		panic(fmt.Sprintf("Type %s cannot be assigned to %s", val.Type().Out(0), service))
	}

	b.services[service] = serviceDescriptor{ctr, constructor, scope, nilIfNoop(destroy)}
	return b
}

func addSingleton(b *serviceCollection, service reflect.Type, inst interface{}) ServiceCollection {
	b.services[service] = serviceDescriptor{inst, instance, singleton, nil}
	return b
}

func nilIfNoop(destroy func(instance interface{})) func(instance interface{}) {
	if destroy == nil || reflect.ValueOf(destroy).Pointer() == reflect.ValueOf(Noop).Pointer() {
		return nil
	} else {
		return destroy
	}
}
