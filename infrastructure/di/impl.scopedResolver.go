package di

import (
	"fmt"
	"reflect"
	"sync"
)

type buildingTask chan interface{}

type destroyable struct {
	instance interface{}
	destroy  func(interface{})
}

func createScopedResolver(e *serviceEngine) *scopedResolver {
	return &scopedResolver{
		engine:   e,
		resolved: map[reflect.Type]interface{}{},
		destroy:  make([]destroyable, 0),
	}
}

// implement scoped resolver

type scopedResolver struct {
	engine   *serviceEngine
	resolved map[reflect.Type]interface{}
	destroy  []destroyable
	mux      sync.Mutex
}

func (s *scopedResolver) Resolve(service reflect.Type) interface{} {
	inst, err := resolve(s, service)
	if err != nil {
		panic(err)
	}
	return inst
}

func (s *scopedResolver) Destroy() {
	for _, item := range s.destroy {
		item.destroy(item.instance)
	}

	s.resolved = nil
	s.destroy = nil
	s.engine = nil
}

func resolve(scope *scopedResolver, service reflect.Type) (interface{}, error) {
	activator := scope.engine.getActivator(service)
	if activator.lifetime == singleton {
		scope = scope.engine.scope
	}
	ensureActivationAllowed(scope, activator, service)
	return createInstance(scope, activator, service)
}

func createInstance(scope *scopedResolver, activator serviceActivator, service reflect.Type) (interface{}, error) {
	building, inst, exists := tryGetResolved(scope, service, activator.lifetime)
	if exists {
		return inst, nil
	}

	return createInstanceAndNotify(activator, scope, service, building)
}

func createInstanceAndNotify(activator serviceActivator, scope *scopedResolver, service reflect.Type, building buildingTask) (interface{}, error) {
	inst, err := activator.factory(scope)

	if err != nil {
		scope.mux.Lock()
		delete(scope.resolved, service)
		scope.mux.Unlock()
	} else {
		registerService(scope, service, inst, activator)
	}

	if building != nil {
		close(building)
	}

	return inst, err
}

func tryGetResolved(scope *scopedResolver, service reflect.Type, lifetime lifetime) (buildingTask, interface{}, bool) {
	if lifetime == transient {
		return nil, nil, false
	}

	scope.mux.Lock()

	if inst, ok := scope.resolved[service]; ok {
		scope.mux.Unlock()

		if building, isBuilding := inst.(buildingTask); isBuilding {
			return building, awaitActivation(building, scope, service), true
		} else {
			return building, inst, true
		}
	}

	building := make(buildingTask)
	scope.resolved[service] = building

	scope.mux.Unlock()

	return building, nil, false
}

func awaitActivation(building buildingTask, scope *scopedResolver, service reflect.Type) interface{} {
	<-building

	scope.mux.Lock()
	defer scope.mux.Unlock()

	if inst, ok := scope.resolved[service]; ok {
		return inst
	} else {
		// better to store and rethrow original error but it's difficult to implement
		panic(fmt.Sprintf("Other thread failed to create instance of type %s", service))
	}
}

func ensureActivationAllowed(scope *scopedResolver, activator serviceActivator, service reflect.Type) {
	if scope == scope.engine.scope {
		if activator.lifetime == transient && activator.destroy != nil {
			panic(fmt.Sprintf("Trying to resolve transient service of type %s with destroyer in root scope. This will lead to memory leak. Maybe you want to use single instance?", service))
		}

		if activator.lifetime == scoped {
			panic(fmt.Sprintf("Service of type %s cannot be created outside of scope", service))
		}
	}
}

func registerService(scope *scopedResolver, service reflect.Type, inst interface{}, activator serviceActivator) {
	scope.mux.Lock()
	defer scope.mux.Unlock()

	if activator.destroy != nil {
		destroyer := destroyable{instance: inst, destroy: activator.destroy}
		scope.destroy = append(scope.destroy, destroyer)
	}

	if activator.lifetime != transient {
		scope.resolved[service] = inst
	}
}
