package di

import (
	"gotodo/utils/rw"
	"net/http"
)

type ctx_ int

const ctxKey = ctx_(1)

func Middleware(root ServiceEngine) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			scope := root.Scope()
			defer scope.Destroy()
			next.ServeHTTP(w, rw.AddContextValue(r, ctxKey, scope))
		})
	}
}

func GetResolver(r *http.Request) Resolver {
	return r.Context().Value(ctxKey).(ScopedResolver)
}
