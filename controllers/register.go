package controllers

import (
	"gotodo/features/todo"
	"gotodo/infrastructure/routing"
	"net/http"
	"reflect"
)

func RegisterTo(r routing.Router) {
	service := reflect.TypeOf((*todo.TodoService)(nil))
	controller := r.NewController("/todos", service)

	controller.Register("", "GetAll", http.MethodGet, routing.Params())
	controller.Register("", "Create", http.MethodPost, routing.Params().Body())
	controller.Register("panic", "LetsPanic", http.MethodGet, routing.Params())
	controller.Register("{id:[0-9]+}", "Get", http.MethodGet, routing.Params().Path("id"))
	controller.Register("{id:[0-9]+}", "Update", http.MethodPut, routing.Params().Path("id").Body())
	controller.Register("{id:[0-9]+}", "Delete", http.MethodDelete, routing.Params().Path("id"))
}
