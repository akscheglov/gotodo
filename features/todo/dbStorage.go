package todo

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type dbStorage struct {
	db *gorm.DB
}

func NewDbStorage(db *gorm.DB) *dbStorage {
	return &dbStorage{db}
}

func (s *dbStorage) FindById(id int) *Todo {
	var todo Todo
	s.db.Where(Todo{ID: id}).Take(&todo)
	if todo.ID == 0 {
		return nil
	}
	return &todo
}

func (s *dbStorage) Create(todo *Todo) {
	s.db.NewRecord(*todo)
	s.db.Create(todo)
}

func (s *dbStorage) All() *[]Todo {
	var todos []Todo
	s.db.Find(&todos)
	return &todos
}

func (s *dbStorage) Delete(id int) {
	s.db.Where(Todo{ID: id}).Delete(Todo{})
}

func (s *dbStorage) Update(todo *Todo) {
	s.db.Model(&todo).Where(Todo{ID: todo.ID}).Updates(todo)
}
