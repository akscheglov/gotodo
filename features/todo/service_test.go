package todo

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetAll(t *testing.T) {
	s := service()

	expected := []Todo{
		{1, "foo", true},
		{2, "bar", false},
		{3, "baz", false},
	}

	actual := s.GetAll()

	assert.Equal(t, expected, *actual)
}

func TestGet(t *testing.T) {
	s := service()

	expected := Todo{2, "bar", false}

	actual := s.Get(2)

	assert.Equal(t, expected, *actual)
}

func TestCreate(t *testing.T) {
	s := service()

	expectedCreated := Todo{4, "hello", false}
	expectedAll := []Todo{
		{1, "foo", true},
		{2, "bar", false},
		{3, "baz", false},
		{4, "hello", false},
	}

	created := s.Create(CreateTodoModel{"hello"})
	assert.Equal(t, expectedCreated, created)

	all := s.GetAll()
	assert.Equal(t, expectedAll, *all)
}

func TestDelete(t *testing.T) {
	s := service()

	expectedAll := []Todo{
		{1, "foo", true},
		{3, "baz", false},
	}

	s.Delete(2)

	all := s.GetAll()
	assert.Equal(t, expectedAll, *all)
}

func TestUpdate(t *testing.T) {
	s := service()

	expectedUpdated := Todo{3, "hello update", true}
	expectedAll := []Todo{
		{1, "foo", true},
		{2, "bar", false},
		{3, "hello update", true},
	}

	updated := s.Update(3, UpdateTodoModel{"hello update", true})
	assert.Equal(t, expectedUpdated, *updated)

	all := s.GetAll()
	assert.Equal(t, expectedAll, *all)
}

func TestUpdateWhenNotFound(t *testing.T) {
	s := service()

	expectedAll := []Todo{
		{1, "foo", true},
		{2, "bar", false},
		{3, "baz", false},
	}

	updated := s.Update(8, UpdateTodoModel{"hello update", true})
	assert.Nil(t, updated)

	all := s.GetAll()
	assert.Equal(t, expectedAll, *all)
}

func service() *TodoService {
	s := New(NewMemStorage(), nil)
	return s
}
