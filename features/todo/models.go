package todo

type Todo struct {
	ID         int    `json:"id"`
	Title      string `json:"title"`
	IsComplete bool   `json:"isComplete"`
}

type CreateTodoModel struct {
	Title string `json:"title" validate:"required"`
}

type UpdateTodoModel struct {
	Title      string `json:"title" validate:"required"`
	IsComplete bool   `json:"isComplete"`
}
