package todo

import (
	"gotodo/infrastructure/logger"
	"reflect"
	"time"
)

var ServiceType = reflect.TypeOf((*TodoService)(nil))

type TodoService struct {
	storage Storage
	log     logger.Logger
}

func New(storage Storage, log logger.Logger) *TodoService {
	return &TodoService{storage, log}
}

func (s *TodoService) GetAll() *[]Todo {
	if s.log != nil {
		s.log.Debug().Msg("Getting all...")
	}
	<-time.After(10 * time.Millisecond)
	return s.storage.All()
}

func (s *TodoService) Get(id int) *Todo {
	return s.storage.FindById(id)
}

func (s *TodoService) Create(model CreateTodoModel) Todo {
	todo := Todo{Title: model.Title}
	s.storage.Create(&todo)
	return todo
}

func (s *TodoService) Update(id int, model UpdateTodoModel) *Todo {
	todo := s.storage.FindById(id)
	if todo != nil {
		todo.Title = model.Title
		todo.IsComplete = model.IsComplete
		s.storage.Update(todo)
	}
	return todo
}

func (s *TodoService) Delete(id int) {
	s.storage.Delete(id)
}

func (s *TodoService) LetsPanic() {
	panic("FIRE!")
}
