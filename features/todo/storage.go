package todo

import "reflect"

var StorageType = reflect.TypeOf((*Storage)(nil)).Elem()

type Storage interface {
	FindById(id int) *Todo
	Create(todo *Todo)
	All() *[]Todo
	Delete(id int)
	Update(todo *Todo)
}
