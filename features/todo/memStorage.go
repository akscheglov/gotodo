package todo

import (
	"gotodo/utils/nums"
)

type memStorage struct {
	todos *[]Todo
}

func NewMemStorage() *memStorage {
	return &memStorage{todos: &[]Todo{
		{1, "foo", true},
		{2, "bar", false},
		{3, "baz", false},
	}}
}

func (s *memStorage) FindById(id int) *Todo {
	todo, _ := find(s.todos, id)
	return todo
}

func (s *memStorage) Create(todo *Todo) {
	todo.ID = nextid(s.todos)
	todos := append(*s.todos, *todo)
	s.todos = &todos
}

func (s *memStorage) All() *[]Todo {
	return s.todos
}

func (s *memStorage) Delete(id int) {
	todo, idx := find(s.todos, id)

	if todo != nil {
		todos := *s.todos
		todos = append(todos[:idx], todos[idx+1:]...)
		s.todos = &todos
	}
}

func (s *memStorage) Update(todo *Todo) {
	found, _ := find(s.todos, todo.ID)

	if found != nil {
		found.Title = todo.Title
	}
}

func find(todos *[]Todo, id int) (*Todo, int) {
	for idx := 0; idx < len(*todos); idx++ {
		todo := &(*todos)[idx]
		if todo.ID == id {
			return todo, idx
		}
	}

	return nil, -1
}

func nextid(todos *[]Todo) int {
	id := 0
	for _, todo := range *todos {
		id = nums.Max(id, todo.ID)
	}
	return id + 1
}
