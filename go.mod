module gotodo

go 1.13

require (
	github.com/ahmetb/go-linq/v3 v3.1.0 // indirect
	github.com/aphistic/golf v0.0.0-20180712155816-02c07f170c5a // indirect
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/go-playground/validator/v10 v10.0.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/halink0803/zerolog-graylog-hook v0.0.0-20180726102656-e138ca268b12
	github.com/jinzhu/gorm v1.9.11
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/justinas/alice v1.2.0
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/rakyll/hey v0.1.2 // indirect
	github.com/rs/zerolog v1.17.2
	github.com/sarulabs/di v2.0.0+incompatible // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/viper v1.6.1
	github.com/stretchr/testify v1.4.0
	github.com/thoas/go-funk v0.5.0 // indirect
	github.com/zenazn/goji v0.9.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.2 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
